--CREATE DATABASE [T_S_module_3];


USE [T_S_module_3]
go
CREATE TABLE [Animal] (
ID			int		identity Primary Key,     
group_anml		varchar(15),
subgroup_anml	varchar(15),
anml	varchar(15), 
kind	varchar(30),
sex	char(1),
birth_date	date,
age	int,
--picture_1st	blob,
passport	varchar(15),
name varchar(250),
master	varchar(50),
master_phone varchar (15),
chip varchar (3),
register_date date,
add_inf_master text
)



CREATE TABLE [visit] (
ID			int	identity	Primary Key,
id_animal int,     
weight_anml		float(1),
prev_vis_date	date,
plan_vis_date	date, 
plan_vis_time	datetime,
plan_vis_issue text,
complaint	text,
diagnosis text,
--picture	blob,
medicines	text,
price float (2),
    CONSTRAINT  FK_visit_animals   FOREIGN KEY  (id_animal)
    REFERENCES  Animal  (id)
    ON DELETE  CASCADE       ON UPDATE  SET NULL

)

--select * from Animal;
INSERT  INTO   Animal (group_anml,subgroup_anml,anml, kind,sex,
birth_date,age,passport,name,master,
master_phone ,chip ,register_date,add_inf_master)
VALUES  ('chord',
'mammal', 
'pig', 
'chinees',
'f', 
'12.12.1998', 
'44', 
' ',
'Roarr',
'Tormoss S.',
'0322223322', 
'yes',
'10.11.2005',
'murderer') 
 
ALTER TABLE  Animal   
   ADD  CONSTRAINT  CK_Animal_group  
   CHECK (group_anml = 'chord'or group_anml='hordless') 
   
   
   select anml from Animal
   where chip='no'

   select * from Animal
   